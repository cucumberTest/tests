$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("app.feature");
formatter.feature({
  "line": 1,
  "name": "test APIs",
  "description": "",
  "id": "test-apis",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "client makes call to GET /response",
  "description": "",
  "id": "test-apis;client-makes-call-to-get-/response",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "the client calls /status",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "the client calls /call",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "APIresponse returns OK",
  "keyword": "And "
});
formatter.match({
  "location": "ResponseResults.the_APIcall_and_APIresponse_are_running()"
});
formatter.result({
  "duration": 490099881,
  "status": "passed"
});
formatter.match({
  "location": "ResponseResults.the_client_calls_call()"
});
formatter.result({
  "duration": 257843235,
  "status": "passed"
});
formatter.match({
  "location": "ResponseResults.apiresponse_returns_OK()"
});
formatter.result({
  "duration": 18062516,
  "status": "passed"
});
});