package hello;

import org.junit.Assert;
import org.springframework.web.client.RestTemplate;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ResponseResults {

    String responseString;
    String testResponse;
    RestTemplate template = new RestTemplate();
    private final String callUrl = "http://localhost:9005";
    private final String responseUrl = "http://localhost:9006";
    private final String exceptionUrl = "http://localhost:9005";

    @Given("^the client calls /status$")
    public void the_APIcall_and_APIresponse_are_running() throws Throwable {
        responseString = template.getForObject(exceptionUrl  + "/status", String.class);
        //Assert.assertEquals("200", testResponse);
    }

    @When("^the client calls /call$")
    public void the_client_calls_call() throws Throwable {
        responseString = template.getForObject(callUrl  + "/call", String.class);
        System.out.println(responseString);

    }


    @Then("^APIresponse returns OK$")
    public void apiresponse_returns_OK() throws Throwable {
        testResponse = template.getForObject(responseUrl + "/response", String.class);
        System.out.println(testResponse);
        Assert.assertEquals("OK", testResponse);

    }

}
